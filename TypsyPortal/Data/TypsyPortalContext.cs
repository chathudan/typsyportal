using System;
using Microsoft.EntityFrameworkCore;
using TypsyPortal.Models;

namespace TypsyPortal.Data
{
    public class TypsyPortalContext : DbContext
    {
        public TypsyPortalContext(DbContextOptions<TypsyPortalContext> options)
            : base(options)
        {
        }

        public DbSet<Profile> Profile { get; set; }

        public DbSet<TypsyPortal.Models.Category> Category { get; set; }
    }
}
