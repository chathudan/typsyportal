﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TypsyPortal.Models
{
    public class Category
    {
        public int Id { get; set; }

        [Required] 
        public string name { get; set; }

        public StatusType Status { get; set; }

        public enum StatusType : byte { active = 1, archived = 0 }
    }
}
