﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TypsyPortal.Models
{
    public class Profile
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(15, ErrorMessage = "Maximum {1} characters.")]
        public string Firstname { get; set; }

        [Required]
        [MaxLength(20, ErrorMessage = "Maximum {1} characters.")]
        public string Lastname { get; set; }

        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,10})+)$",ErrorMessage = "Please enter a valid email address.")]
        [Required]
        [MaxLength(50, ErrorMessage = "Maximum {1} characters.")]
        public string Email { get; set; }

        [DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Birthday { get; set; }

        [Display(Description = "How did you find out about us?")]
        public ReferralType Referral { get; set; }

        [MaxLength(200, ErrorMessage = "Maximum {1} characters.")]
        public string ReferralReason { get; set; }

        [Required]
        [MaxLength(15, ErrorMessage = "Maximum {1} characters.")]
        public string Phone { get; set; }

        [Required]
        public string Country { get; set; }

        public GenderType Gender { get; set; }

        //public byte Status { get; set; }

        [NotMapped]
        public string Name
        {
            get { return $"{Firstname} {Lastname}"; }
        }

        public enum ReferralType : byte { SocialMedia = 1, OnlineSearch = 2, Events = 3, Advertising = 4, Other = 5 }

        public enum GenderType : byte { Undisclosed = 0, Male = 1, Female = 2 }
    }
}
