﻿$(document).ready(function () {
    var fname_max = 15;
    var lname_max = 20;
    var phone_max = 15;
    var email_max = 50;
    var referral_reason_max = 200;

    // First name validation
    $('#first_name').keyup(function () {
        var text_length = $('#first_name').val().length;
        var text_remaining = fname_max - text_length;

        $('#spn_first_name').show();
        $('#spn_first_name').html(text_remaining + ' character(s) remaining');
    });

    // Last name validation
    $('#last_name').keyup(function () {
        var text_length = $('#last_name').val().length;
        var text_remaining = lname_max - text_length;

        $('#spn_last_name').html(text_remaining + ' character(s) remaining');
    });

    // email validation  
    $('#email').keyup(function () {
        var text_length = $('#email').val().length;
        var text_remaining = email_max - text_length;

        $('#spn_email').html(text_remaining + ' character(s) remaining');
    });

    // Phone number validation
    $('#phone').keyup(function () {
        var text_length = $('#phone').val().length;
        var text_remaining = phone_max - text_length;

        $('#spn_phone').html(text_remaining + ' character(s) remaining');
    });

    //Referral validation 
    $('#select_referral').change(function () {
        if ($(this).val() == '5') {
            $('#form_group_referral_reason').show();
        }
        else {
            $('#form_group_referral_reason').hide();
        }
    });

    //Referral reason validation  
    $('#referral_reason').keyup(function () {
        var text_length = $('#referral_reason').val().length;
        var text_remaining = referral_reason_max - text_length;

        $('#spn_referral_reason').html(text_remaining + ' character(s) remaining');
    });

});

function CheckEmail(profileEmail) { 

    var Email = $("#email").val();
    var SpanEmail = $("#spn_email")

    if (profileEmail != Email) {
        $.ajax({
            url: '/Profile/CheckEmail',
            data: { Email: Email },
            type: 'POST',
            dataType: 'json',
            success: function (result) {
                if (result) {
                    SpanEmail.html('Email laready exist!');
                }
                else { 
                }
            }
        });
    }
}