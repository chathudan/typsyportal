# IO.Swagger.Api.ProfileApi

All URIs are relative to *https://typsyportal.azurewebsites.net/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddProfile**](ProfileApi.md#addprofile) | **POST** /profile | Add a new profile
[**DeleteProfile**](ProfileApi.md#deleteprofile) | **DELETE** /profile/{profileId} | Deletes a profile
[**GetProfileById**](ProfileApi.md#getprofilebyid) | **GET** /profile/{profileId} | Find profile by ID
[**UpdateProfile**](ProfileApi.md#updateprofile) | **PUT** /profile | Update an existing profile


<a name="addprofile"></a>
# **AddProfile**
> void AddProfile (Profile body)

Add a new profile

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class AddProfileExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("api_key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("api_key", "Bearer");

            var apiInstance = new ProfileApi();
            var body = new Profile(); // Profile | Pet object that needs to be added to the store

            try
            {
                // Add a new profile
                apiInstance.AddProfile(body);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ProfileApi.AddProfile: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Profile**](Profile.md)| Pet object that needs to be added to the store | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deleteprofile"></a>
# **DeleteProfile**
> void DeleteProfile (long? profileId)

Deletes a profile

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class DeleteProfileExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("api_key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("api_key", "Bearer");

            var apiInstance = new ProfileApi();
            var profileId = 789;  // long? | Profile id to delete

            try
            {
                // Deletes a profile
                apiInstance.DeleteProfile(profileId);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ProfileApi.DeleteProfile: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profileId** | **long?**| Profile id to delete | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getprofilebyid"></a>
# **GetProfileById**
> Profile GetProfileById (long? profileId)

Find profile by ID

Returns a single profile

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class GetProfileByIdExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("api_key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("api_key", "Bearer");

            var apiInstance = new ProfileApi();
            var profileId = 789;  // long? | ID of profile to return

            try
            {
                // Find profile by ID
                Profile result = apiInstance.GetProfileById(profileId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ProfileApi.GetProfileById: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profileId** | **long?**| ID of profile to return | 

### Return type

[**Profile**](Profile.md)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updateprofile"></a>
# **UpdateProfile**
> void UpdateProfile (Profile body)

Update an existing profile

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UpdateProfileExample
    {
        public void main()
        {
            // Configure API key authorization: api_key
            Configuration.Default.AddApiKey("api_key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("api_key", "Bearer");

            var apiInstance = new ProfileApi();
            var body = new Profile(); // Profile | Pet object that needs to be added to the store

            try
            {
                // Update an existing profile
                apiInstance.UpdateProfile(body);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ProfileApi.UpdateProfile: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Profile**](Profile.md)| Pet object that needs to be added to the store | 

### Return type

void (empty response body)

### Authorization

[api_key](../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

