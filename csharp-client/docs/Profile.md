# IO.Swagger.Model.Profile
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **long?** |  | [optional] 
**FirstName** | **string** |  | [optional] 
**LasttName** | **string** |  | [optional] 
**Email** | **string** |  | [optional] 
**Bithday** | **int?** |  | [optional] 
**Referral** | **int?** |  | [optional] 
**Country** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

