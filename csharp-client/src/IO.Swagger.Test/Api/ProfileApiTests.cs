/* 
 * Typsy Portal
 *
 *  Typsy portal APIs
 *
 * OpenAPI spec version: 1.0.0
 * Contact: info@abc.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using RestSharp;
using NUnit.Framework;

using IO.Swagger.Client;
using IO.Swagger.Api;
using IO.Swagger.Model;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing ProfileApi
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the API endpoint.
    /// </remarks>
    [TestFixture]
    public class ProfileApiTests
    {
        private ProfileApi instance;

        /// <summary>
        /// Setup before each unit test
        /// </summary>
        [SetUp]
        public void Init()
        {
            instance = new ProfileApi();
        }

        /// <summary>
        /// Clean up after each unit test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of ProfileApi
        /// </summary>
        [Test]
        public void InstanceTest()
        {
            // TODO uncomment below to test 'IsInstanceOfType' ProfileApi
            //Assert.IsInstanceOfType(typeof(ProfileApi), instance, "instance is a ProfileApi");
        }

        
        /// <summary>
        /// Test AddProfile
        /// </summary>
        [Test]
        public void AddProfileTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //Profile body = null;
            //instance.AddProfile(body);
            
        }
        
        /// <summary>
        /// Test DeleteProfile
        /// </summary>
        [Test]
        public void DeleteProfileTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //long? profileId = null;
            //instance.DeleteProfile(profileId);
            
        }
        
        /// <summary>
        /// Test GetProfileById
        /// </summary>
        [Test]
        public void GetProfileByIdTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //long? profileId = null;
            //var response = instance.GetProfileById(profileId);
            //Assert.IsInstanceOf<Profile> (response, "response is Profile");
        }
        
        /// <summary>
        /// Test UpdateProfile
        /// </summary>
        [Test]
        public void UpdateProfileTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //Profile body = null;
            //instance.UpdateProfile(body);
            
        }
        
    }

}
